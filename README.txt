____________________________________________________________________________________________________________________________

	PseudoSpectral GPU codes for DNS of the Navier-Stokes equations
____________________________________________________________________________________________________________________________

	This repository contains DNS codes for the incompressible Navier-Stokes equations in periodic geometeries in 3 different flavours; 
    2D, 3D and 3D with variable density (Boussinesq). 
    All codes utilise GPU acceleration via the CUDA API and use pseudospectral spatial discretisation in all directions.
    See the directories herein for further readme information. 
		
  -----------
  Contact
  -----------

        Authors: Dan Lucas
        Institution: Keele University
        Email:  d.lucas1@keele.ac.uk
	
September 2017
